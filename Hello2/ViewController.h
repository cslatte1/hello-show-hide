//
//  ViewController.h
//  Hello2
//
//  Created by Ciaran Slattery on 1/26/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UILabel *helloWorldLabel;
    IBOutlet UIButton *showButton;
    IBOutlet UIButton *hideButton;
}

-(IBAction)showButton:(id)sender;
-(IBAction)hideButton:(id)sender;


@end

