//
//  ViewController.m
//  Hello2
//
//  Created by Ciaran Slattery on 1/26/17.
//  Copyright © 2017 Ciaran Slattery. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
-(IBAction)showButton:(id)sender
{
    helloWorldLabel.hidden = NO;
    showButton.hidden = YES;
    hideButton.hidden = NO;
}
-(IBAction)hideButton:(id)sender
{
    helloWorldLabel.hidden = YES;
    showButton.hidden = NO;
    hideButton.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    helloWorldLabel.hidden = YES;
    showButton.hidden = NO;
    hideButton.hidden = YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
